<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		 xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>com.nielsenmedia.stationmap</groupId>
	<artifactId>station-map</artifactId>
	<version>1.0</version>
	<packaging>jar</packaging>

	<properties>
		<maven.compiler.source>1.8</maven.compiler.source>
		<maven.compiler.target>1.8</maven.compiler.target>
		<encoding>UTF-8</encoding>
		<aws.java.sdk.version>1.11.79</aws.java.sdk.version>
		<javax.mail.version>1.4.1</javax.mail.version>
		<junit.version>4.11</junit.version>
		<hadoop.version>2.8.1</hadoop.version>
		<scala.binary.version>2.11.8</scala.binary.version>
		<scala.compat.version>2.11</scala.compat.version>
		<scala.test.version>3.0.5</scala.test.version>
		<spark.testing.base.version>0.9.0</spark.testing.base.version>
		<spark.version>2.1.1</spark.version>
		<typesafe.config>1.3.2</typesafe.config>
		<maven.surefire.version>2.20.1</maven.surefire.version>
	</properties>

	<!-- used by buildnumber-maven-plugin -->
	<scm>
		<connection>scm:git:git@gitlab.com:arunkumar.thirunavakarasu.consultant/scarborough_station_map.git</connection>
	</scm>

	<distributionManagement>
		<repository>
			<id>adlm-artifactory-connection</id>
			<name>artifactory-releases</name>
			<url>https://artifactory.adlm.nielsen.com:443/artifactory/watch-tam-libs-release</url>
		</repository>
		<snapshotRepository>
			<id>adlm-artifactory-connection</id>
			<name>artifactory-snapshots</name>
			<url>https://artifactory.adlm.nielsen.com:443/artifactory/watch-tam-libs-snapshot</url>
		</snapshotRepository>
	</distributionManagement>

	<dependencies>
		<dependency>
			<groupId>org.apache.spark</groupId>
			<artifactId>spark-hive_${scala.compat.version}</artifactId>
			<version>${spark.version}</version>
		</dependency>

		<dependency>
			<groupId>com.amazonaws</groupId>
			<artifactId>aws-java-sdk</artifactId>
			<version>${aws.java.sdk.version}</version>
		</dependency>

		<dependency>
			<groupId>javax.mail</groupId>
			<artifactId>mail</artifactId>
			<version>${javax.mail.version}</version>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>com.typesafe</groupId>
			<artifactId>config</artifactId>
			<version>${typesafe.config}</version>
		</dependency>

		<!-- Testing -->
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>${junit.version}</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.scalatest</groupId>
			<artifactId>scalatest_${scala.compat.version}</artifactId>
			<version>${scala.test.version}</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>com.holdenkarau</groupId>
			<artifactId>spark-testing-base_${scala.compat.version}</artifactId>
			<version>${spark.version}_${spark.testing.base.version}</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.scalamock</groupId>
			<artifactId>scalamock-scalatest-support_2.11</artifactId>
			<version>3.3.0</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>com.holdenkarau</groupId>
			<artifactId>spark-testing-base_2.11</artifactId>
			<version>${spark.version}_0.10.0</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.mockito</groupId>
			<artifactId>mockito-all</artifactId>
			<version>1.9.5</version>
			<scope>test</scope>
		</dependency>

	</dependencies>

	<build>
		<sourceDirectory>${project.basedir}/src/main/scala</sourceDirectory>
		<testSourceDirectory>${project.basedir}/src/test/scala</testSourceDirectory>

		<resources>
			<resource>
				<directory>${project.basedir}/src/main/resources</directory>
				<includes>
					<include>*.conf</include>
				</includes>
				<filtering>true</filtering>
			</resource>
			<resource>
				<directory>${project.basedir}/src/main/resources/properties</directory>
				<includes>
					<include>*.prop</include>
				</includes>
				<filtering>true</filtering>
			</resource>
		</resources>
		<testResources>
			<testResource>
				<directory>${project.basedir}/src/test/resources</directory>
				<includes>
					<include>*.conf</include>
				</includes>
				<filtering>true</filtering>
			</testResource>
		</testResources>
		<plugins>
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>buildnumber-maven-plugin</artifactId>
				<version>1.4</version>
				<executions>
					<execution>
						<phase>validate</phase>
						<goals>
							<goal>create</goal>
						</goals>
					</execution>
				</executions>
				<configuration>
					<scmBranchPropertyName>buildScmBranch</scmBranchPropertyName>
					<doCheck>false</doCheck>
					<doUpdate>false</doUpdate>
				</configuration>
			</plugin>
			<plugin>
				<!-- see http://davidb.github.com/scala-maven-plugin -->
				<groupId>net.alchim31.maven</groupId>
				<artifactId>scala-maven-plugin</artifactId>
				<version>3.3.1</version>
				<executions>
					<execution>
						<goals>
							<goal>compile</goal>
							<goal>testCompile</goal>
						</goals>
						<configuration>
							<args>
								<!-- REMOVE THIS LINE: <arg>-make:transitive</arg> -->
								<arg>-dependencyfile</arg>
								<arg>${project.build.directory}/.scala_dependencies</arg>
							</args>
						</configuration>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<!-- add -Dmaven.test.skip=true to skip tests -->
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<version>${maven.surefire.version}</version>
				<configuration>
					<!-- If you have classpath issue like NoDefClassError,... -->
					<!-- useManifestOnlyJar>false</useManifestOnlyJar -->
					<includes>
						<include>**/*Test.*</include>
						<include>**/*Suite.*</include>
					</includes>
					<!-- Suggestion from spark-testing-base -->
					<forkCount>1</forkCount>
					<reuseForks>false</reuseForks>
					<!--<argLine>-Xmx2048m -XX:MaxPermSize=2048m</argLine>-->
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.honton.chas</groupId>
				<artifactId>exists-maven-plugin</artifactId>
				<version>0.0.6</version>
				<executions>
					<execution>
						<goals>
							<goal>remote</goal>
						</goals>
						<configuration>
							<artifact>${project.artifactId}-${project.version}-${environment.name}.${project.packaging}</artifact>
							<failIfExists>true</failIfExists>
						</configuration>
					</execution>
				</executions>
			</plugin>

			<!--
				The following configuration of jar plugin and assembly plugin allows to avoid
				building the Primary Artifact (which is an artifact without classifier)
				and to override artifact produced by jar plugin (w/o dependencies)
				with the one produced by assembly plugin (w dependencies).

				While jar plugin and assembly plugin are producing jars with classifiers,
				references to this jars are being added to the Attached Artifacts list.
				That leads to two equal entries in that list.

				To avoid processing the same jar twice, <attach>false</attach> was added.
				Keep in mind that this also hides the following message:
				"[WARNING] Replacing pre-existing project main-artifact file X with assembly file Y"
			-->
			<plugin>
				<artifactId>maven-jar-plugin</artifactId>
				<version>3.1.0</version>
				<configuration>
					<classifier>${environment.name}</classifier>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-assembly-plugin</artifactId>
				<version>3.1.0</version>
				<configuration>
					<descriptorRefs>
						<descriptorRef>jar-with-dependencies</descriptorRef>
					</descriptorRefs>
					<archive>
						<manifestEntries>
							<Project-Branch>${buildScmBranch}</Project-Branch>
							<Project-Last-Commit>${buildNumber}</Project-Last-Commit>
							<Project-Version>${project.version}</Project-Version>
							<Project-Environment-Name>${environment.name}</Project-Environment-Name>
						</manifestEntries>
					</archive>
					<finalName>${project.artifactId}-${project.version}-${environment.name}</finalName>
					<appendAssemblyId>false</appendAssemblyId>
					<attach>false</attach>
				</configuration>
				<executions>
					<execution>
						<phase>package</phase>
						<goals>
							<goal>single</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>
	<profiles>
		<profile>
			<id>dev</id>
			<properties>
				<environment.name>dev</environment.name>
			</properties>
			<activation>
				<activeByDefault>true</activeByDefault>
			</activation>
		</profile>
		<profile>
			<id>uat</id>
			<properties>
				<environment.name>uat</environment.name>
			</properties>
		</profile>
		<profile>
			<id>prl</id>
			<properties>
				<environment.name>prl</environment.name>
			</properties>
		</profile>
		<profile>
			<id>preprod</id>
			<properties>
				<environment.name>preprod</environment.name>
			</properties>
		</profile>
		<profile>
			<id>prod</id>
			<properties>
				<environment.name>prod</environment.name>
			</properties>
		</profile>
	</profiles>
</project>
