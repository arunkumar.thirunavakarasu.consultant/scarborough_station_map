package com.nielsenmedia.stationmap.config

import java.util.logging.Logger

import com.nielsenmedia.stationmap.util.Util
import com.typesafe.config.ConfigException.Missing
import com.typesafe.config.{Config, ConfigFactory, ConfigObject}

import scala.collection.mutable

object TableConfigLoader {
  var inputMap: Option[Map[String, String]] = Option.empty
  private val logger = Logger.getLogger(getClass.getName)
  private var config: Option[mutable.HashMap[String, TableConfiguration]] = Option.empty
  var TABLE_CONFIG_NAME_PROP: String = "tables.configurtion"

  /**
    * The method reads configuration from the tables.conf file and returns Option[TableConfiguration].
    *
    * @param tableName table name
    * @return TableConfiguration
    */
  def tableConf(tableName: String): TableConfiguration = {
    val optional = config.getOrElse(initTableConfig()).get(tableName)
    if(optional.isEmpty) {
      throw new NoSuchElementException(s"Configuration for table $tableName does not exists")
    }
    optional.get
  }

  def initTableConfig(): mutable.HashMap[String, TableConfiguration] = {
    val c = load()
    val default = c.getConfig("default")
    val map = new mutable.HashMap[String, TableConfiguration]()

    c.getObjectList("tablesList")
      .toArray
      .foreach(tc => {
        val tc1 = tc.asInstanceOf[ConfigObject].toConfig.withFallback(default)
        map.put(tc1.getString("tableName"), tableConfigFromConfig(tc1))
      })
    config = Option(map)
    map
  }


  /**
    * Creates TableConfiguration class from the Config
    *
    * @param tc table Config
    * @return TableConfiguration class
    */
  private def tableConfigFromConfig(tc: Config): TableConfiguration = {
    val fileFormat = readOptionalString(tc, "fileFormat").get match {
      case "CSV" => FileFormat.CSV
      case _ => FileFormat.PARQUET
    }
    //basePath = ""
    TableConfiguration(tc.getString("tableName"),
      readOptionalString(tc, "dmaPartitionColumnName"),
      readOptionalString(tc, "defaultDmaPartitionId"),
      readOptionalString(tc, "datePartitionColumnName"),
      readOptionalString(tc, "header"),
      fileFormat,
      readOptionalString(tc, "basePath"),
      readOptionalString(tc, "fileName"),
      readOptionalString(tc, "outputFilePath"),
      tc.getInt("partitionNum")
    )
  }

  def apply(inputMap: Map[String, String]): Unit = {
    TableConfigLoader.inputMap = Option(inputMap)
    println("init imput map")
  }

  /**
    * Gets Optional string by name from the table config.
    * In case the property value equals to null or empty string the method will return Optional.empty.
    *
    * @param c   table Config
    * @param str name of the property you read
    * @return value of the property
    */
  private def readOptionalString(c: Config, str: String): Option[String] = {
    var res = ""
    try {
      res = c.getString(str)
      res = inputMap match {
        case Some(map) => Util.updateTemplateWithInputParameters(res, inputMap.get)
        case None => res
      }
    } catch {
      case e: Missing => logger.warning(s"tables.conf is missing $str property")
    }
    if ("".equals(res)) Option.empty else Option(res)
  }

  /**
    * Loads table config
    *
    * @return
    */
  private def load(): Config = {
    ConfigFactory.load(Option(System.getProperty(TABLE_CONFIG_NAME_PROP)).getOrElse("tables.conf"))
  }

}
