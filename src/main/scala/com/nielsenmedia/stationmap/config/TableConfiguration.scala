package com.nielsenmedia.stationmap.config

import com.nielsenmedia.stationmap.config.FileFormat.FileFormat
import com.nielsenmedia.stationmap.util.Util

case class TableConfiguration(tableName: String,
                              dmaPartitionColumnName: Option[String],
                              defaultDmaPartitionId: Option[String],
                              datePartitionColumnName: Option[String],
                              header: Option[String],
                              fileFormat: FileFormat = FileFormat.PARQUET,
                              basePath: Option[String] = Option.empty,
                              fileName: Option[String] = Option.empty,
                              outputFilePath: Option[String] = Option.empty,
                              partitionNum: Integer = 1
                             ) {
  def outputFilePath(map: Map[String, String]): String = Util.updateTemplateWithInputParameters(outputFilePath.get, map)
  def fileName(map: Map[String, String]): String = Util.updateTemplateWithInputParameters(fileName.getOrElse(tableName), map)
}

object FileFormat extends Enumeration {
  type FileFormat = Value
  val CSV, PARQUET = Value
}
