package com.nielsenmedia.stationmap.config

import com.typesafe.config.{Config, ConfigFactory}

import scala.collection.mutable
import scala.util.{Failure, Success, Try}

/*This code copied from com.nielsenmedia.foundation.lmh.localovernightprojection.process*/
object Configurable extends Serializable {
  val CONFIG_NAME_PROP: String = "config.resource"
  val CONFIG_NAME: String = Option(System.getProperty(CONFIG_NAME_PROP)).getOrElse("rpdplus.conf")
  private val configMap = new mutable.HashMap[String, Config]()
  private val CONFIG_EXTENSION = ".conf"
  private val DEFAULT_SUFFIX = "-default" + CONFIG_EXTENSION
  val PROP_FOLDER_NAME: String = "config.resource"
  val PROP_NAME: String = Option(System.getProperty(PROP_FOLDER_NAME)).getOrElse("default.prop")

  /**
    * This method is entry-point to global "Config" object.
    * Application expects that main configuration file for the job will be provided as "config.resource" system property.
    * First time it will load configuration from this file and cache it. However, at each access, it checks if system property is changed,
    * and if so - previous config will be evicted and new one is loaded. It is handy during unit-tests. Should be constant during
    * running the job.
    * @return
    */
  @SuppressWarnings(Array("org.wartremover.warts.NonUnitStatements"))
  def config: Config = {
    val configName = Option(System.getProperty(CONFIG_NAME_PROP)).getOrElse("rpdplus.conf")
    configMap.getOrElse(configName, {
      configMap.clear()
      val conf = loadConfig(configName)
      configMap.put(configName, conf)
      conf
    })
  }

  /**
    * Load configuration from specified file.
    * If parameter "name" is empty, then default config will be loaded(usually from application.conf or resource.conf)
    * If parameter isn't empty, it tries to load "default" configuration for job.
    * If "config.default" property is not empty, then application will load "default" configuration from here. If property is absent, then
    * application will try to load file add "-default" suffix to the first part of "name" parameter(before the dot).
    * For example, if name="nonRPD.conf", then it will try load configuration from "nonRPD-default.conf" file.
    *
    * If both "default" and "config-by-name" files exist, then "config-by-name" takes higher precedence and overrides "default"
    * configuration. If exist only one of them it will be loaded as main source of configuration(without overrides).
    * If both not found, then IllegalArgumentException will be thrown
    *
    * Examples:
    *
    * 1) In classpath exist "nonRPD.conf" and "nonRPD-default.conf" files, and name = "nonRPD" - values will be retrieved from nonRPD.conf
    * and only in case it not exist in nonRPD.conf, it will try to retrieve it from "nonRPD-defaul.conf".
    * If value missed in both files, the error will be thrown
    * 2) Only "nonRPD.conf" exist in classpath - then no errors will be thrown and values will be retrieved only from "nonRPD.conf"
    * 3) Only "nonRPD-default.conf" exist in classpath - Same as 2)
    * 4) None of files are on classpath, then IllegalArgumentException will be thrown
    *
    * @param name - Filename of main job configuration file. Application expects it in full format(with extension), like  "test.conf"
    * @return configuration object, with fallback to "defaults" for this job(if exist)
    */
  @SuppressWarnings(Array("org.wartremover.warts.Throw"))
  private def loadConfig(name:String) = {
    ConfigFactory.invalidateCaches()
    (loadOverrideConfig, loadDefaultConfig(name)) match {
      case (Some(overrides), Some(default)) => {
        val appConfig = ConfigFactory.parseResources(Option(System.getProperty(CONFIG_NAME_PROP)).getOrElse("rpdplus.conf"))
        appConfig.withFallback(ConfigFactory.parseResources(defaultConfigName(name).getOrElse(""))).resolve()
      }
      case (Some(overrides), None) => overrides
      case (None, Some(default)) => default
      case (None, None) => throw new IllegalArgumentException(s"Configuration for file $name isn't found on classpath, as well as default")
    }
  }

  private def loadOverrideConfig = {
    // Will load file specified in "config.resource" system property
    Try {ConfigFactory.load()} match {
      case Failure(_) => None
      case Success(orig) => Some(orig)
    }
  }

  private def loadDefaultConfig(name: String) = {
    for {
      file <- Option(name)
      default <- defaultConfigName(file)
    } yield ConfigFactory.load(default)
  }

  private def defaultConfigName(filename:String) = {
      Option(filename.replace(CONFIG_EXTENSION, DEFAULT_SUFFIX))
  }
}
