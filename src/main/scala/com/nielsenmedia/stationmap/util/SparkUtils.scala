package com.nielsenmedia.stationmap.util

import org.apache.spark.SparkContext
import org.apache.spark.sql.{DataFrame, SparkSession}

import scala.collection.mutable.ArrayBuffer

/**
  * Contains utils for DataFrames, Hive queries, and other Spark specific structures.
  * Could be split into separate more specific utils if needed.
  *
  * Please, avoid here any code contains business-logic, this must be abstract.
  */
object SparkUtils {

  /**
    * Check whether DataFrame is empty
    * https://stackoverflow.com/a/43383369
    *
    * @param df - DataFrame
    * @return
    */
  def isEmpty(df: DataFrame): Boolean = {
    df.take(1).isEmpty
  }

  /**
    * Print Spark configuration.
    * Additionally, Hadoop configuration, System properties and System Environment could be printed.
    *
    * TODO: consider if this should be done through logger, because println sends the output to
    * stdout which separates debug info from the app logs (they are in stderr), so using println
    * can be more convenient for debugging
    *
    * @param sc        - SparkContext
    * @param hadoopCfg - Should Hadoop configuration be printed (default = false)
    * @param sysProps  - Should system properties be printed (default = false)
    * @param sysEnv    - Should system envirinment be printed (default = false)
    */
  def printCfg(sc: SparkContext,
               hadoopCfg: Boolean = false,
               sysProps: Boolean = false,
               sysEnv: Boolean = false): Unit = {

    println("\n\nConfiguration\n\nSpark config:")
    sc.getConf.getAll.sorted.foreach(println)

    if (hadoopCfg) {
      println("\n\nHadoop config:")
      val hadoopProps = ArrayBuffer[(String, String)]()
      val it = sc.hadoopConfiguration.iterator
      while (it.hasNext) {
        val p = it.next()
        hadoopProps.append((p.getKey, p.getValue))
      }
      hadoopProps.sorted.foreach(println)
    }

    if (sysProps) {
      println("\n\nSystem properties:")
      val sysProps = ArrayBuffer[(String, String)]()

      import scala.collection.JavaConverters._
      for ((k, v) <- System.getProperties.asScala) {
        sysProps.append((k, v))
      }

      sysProps.sorted.foreach(println)
    }

    if (sysEnv) {
      println("\n\nSystem environment:")
      val sysEnv = ArrayBuffer[(String, String)]()

      import scala.collection.JavaConverters._
      for ((k, v) <- System.getenv.asScala) {
        sysEnv.append((k, v))
      }

      sysEnv.sorted.foreach(println)
    }

    println("\n\nEnd Configuration\n\n")
  }

  def setDefaultHadoopConfiguration(sc: SparkContext): Unit = {
    sc.hadoopConfiguration.set("fs.s3a.server-side-encryption-algorithm", "AES256")
  }


  var spark: SparkSession = SparkSession.builder
    .config("spark.sql.crossJoin.enabled", "true")
    .config("spark.sql.parquet.binaryAsString", "true")
    .config("spark.sql.parquet.cacheMetadata", "true")
    .config("spark.sql.parquet.filterPushdown", "true")
    .config("spark.sql.shuffle.partitions", "10")
    .config("spark.sql.sources.partitionColumnTypeInference.enabled", "false")
    .enableHiveSupport()
    .getOrCreate()

}
