package com.nielsenmedia.stationmap.util

import java.net.URI
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.log4j.Logger
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession

object SparkUtil {

  val LOG = Logger.getLogger(getClass)

  val BASE_CONFIG_PROP = "base.config"
  val SPARK_LOCAL_MODE_IS_ENABLED = "localModeEnabled"

  def loadSpark(sparkConf: SparkConf = new SparkConf(), appName: String = null,
                useBaseConfig: Boolean = true): SparkSession = {
    val spark = configureSpark(sparkConf, appName, useBaseConfig)

    if (useBaseConfig) {
      loadBaseConfig(spark.sparkContext.hadoopConfiguration)
    }

    spark
  }

  private def configureSpark(sparkConf: SparkConf, appName: String = null,
                             useBaseConfig: Boolean = true): SparkSession = {
    val sparkBuilder = SparkSession.builder().config(sparkConf)
    if (appName != null) {
      sparkBuilder.appName(appName)
    }

    val localModeEnabled = Option(System.getProperty(SPARK_LOCAL_MODE_IS_ENABLED))
    if (localModeEnabled.isDefined && localModeEnabled.get.equals("true")) {
      sparkBuilder
        .master("local[*]")
        .appName("Test")
        .config("spark.sql.shuffle.partitions", 1)
    }

    sparkBuilder.getOrCreate()
  }

  def loadBaseConfig(conf: Configuration): Unit = {
    val configFile = Option(System.getProperty(BASE_CONFIG_PROP))
    if (configFile.isDefined) {
      val fileName = configFile.get
      loadLocalConfig(conf, fileName)
    }
  }

  /**
    * Loads configuration from local
    *
    * @param conf - hadoop configuration
    * @param configFileName - configuration file name to be loaded
    */
  def loadLocalConfig(conf: Configuration, configFileName: String): Unit = {
    LOG.info(s"Loading config from $configFileName")
    conf.addResource(new Path(configFileName))
  }

  /**
    * Loads configuration from HDFS or S3
    *
    * @param conf - hadoop configuration
    * @param configFileName - configuration file name to be loaded
    */
  def loadExternalConfig(conf: Configuration, configFileName: String): Unit = {
    LOG.info(s"Loading config from $configFileName")
    val fs = FileSystem.get(new URI(configFileName), conf)
    val in = fs.open(new Path(configFileName))
    conf.addResource(in)
  }
}
