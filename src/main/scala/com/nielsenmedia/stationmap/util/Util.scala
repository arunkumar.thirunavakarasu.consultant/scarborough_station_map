package com.nielsenmedia.stationmap.util

import java.io.{File, FileNotFoundException, IOException}
import java.text.ParseException
import java.util._
import java.util.regex.Pattern

import com.nielsenmedia.stationmap.config.Configurable.config
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, FileUtil, Path}
import org.apache.log4j.Logger
import org.apache.spark.SparkContext
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}

import scala.collection.JavaConversions._
import scala.collection.mutable
import scala.collection.mutable.{ListBuffer, _}



object Util {

  val logger = Logger.getLogger(getClass().getName)
  var configuration = new Configuration()
  var fs = FileSystem.get(configuration);

  @throws(classOf[IOException])
  @throws(classOf[NullPointerException])
  def convertDMAtoDCA(dma: String): Integer = {
    val dca = (dma.toInt - 400) * 10
    return dca;
  }
  @throws(classOf[IOException])
  @throws(classOf[NullPointerException])
  def convertDMAtoMetro(dma: String): Integer = (dma.toInt - 400)

  @throws(classOf[IOException])
  def npmMergingOutputDirResolving(dma: String, sscm: String, marketType: String, processingDate: String, configuration: Configuration, fs: FileSystem): String = {
    var dir = new StringBuilder()
    dir.append("stb" + File.separator).append("data" + File.separator).append("output" + File.separator).append(configuration.get("marketType") + File.separator).append(processingDate + File.separator).
      append(Util.convertDMAtoDCA(dma) + File.separator).append(sscm + File.separator).append("output" + File.separator).
      append("24Hrs" + File.separator)
    var fs = FileSystem.get(configuration)
    if (fs.exists(new Path(dir.toString()))) {

      fs.delete(new Path(dir.toString()), true)
    }
    return dir.toString()
  }

  def updateTemplateWithInputParameters(path: String,  map: scala.collection.immutable.Map[String, String]): String = {
    var newPath = path
    for (k <- map.keys) newPath = newPath.replaceAll(s"%${k}%", map(k))
    newPath
  }


  @throws(classOf[IOException])
  def moveDirAndDeleteMergedDir(srcPath: Path, trgtPath: Path, pathPattern: String, configuration: Configuration): Unit = {

    var fs = FileSystem.get(configuration)
    var mergedDir = new Path(srcPath + "_merged")
    logger.info("merged directory is going to delete:" + mergedDir)
    if (fs.exists(mergedDir)) {
      fs.delete(mergedDir, true)
    }

    moveDirMerging(srcPath, trgtPath, pathPattern, configuration)
  }

  @throws(classOf[IOException])
  def moveDirMerging(srcPath: Path, trgtPath: Path, pathPattern: String, configuration: Configuration): Unit = {

    var fs = FileSystem.get(configuration)
    var sourceFilePaths = HadoopUtil.listFiles(srcPath.toString(), pathPattern, true, configuration)
    //delete the Output directory
    HadoopUtil.deleteOutputDirectory(srcPath, sourceFilePaths, trgtPath, configuration)

    for (srcFilePath <- sourceFilePaths) {
      var trgtFilename = srcFilePath.toString().replace(srcPath.toString(), trgtPath.toString());
      var trgtFilePath = new Path(trgtFilename);
      if (logger.isDebugEnabled()) {
        logger.debug("srcFilePath: " + srcFilePath + " - trgtFilePath: " + trgtFilePath);
      }

      if (fs.exists(trgtPath)) {
        fs.delete(trgtPath, true)
      }

      FileUtil.copyMerge(fs, srcPath, fs, trgtPath, false, configuration, "")
    }
    fs.delete(srcPath, true)

  }

    def replaceParamter(inputMap: scala.collection.mutable.Map[String, String], parameterToReplace: String): String = {
      logger.info("Attempting to replace Parameter "+parameterToReplace)
    var replacedParameter = parameterToReplace
    try{
    var pattern = "\\%(.+?)\\%"
    var p = Pattern.compile(pattern);
    var m = p.matcher(parameterToReplace);

    while (m.find()) {
      if (inputMap.contains(m.group(1).toString())) {
        logger.info("replacing "+m.group()+" with "+inputMap(m.group(1)))
        replacedParameter = replacedParameter.replace(m.group(),inputMap(m.group(1)).toString)
      }
      else
      {
        logger.error("Exception occurred while trying to replace the parameter "+m.group()+" as the parameter is not available in the inputMap")
      }
    }
    logger.info("Parameter after replacing "+replacedParameter)
    }
    catch {
      case e: Exception => {
        logger.error("Exception occured while replacing parameter "+parameterToReplace)
        e.printStackTrace()
        System.exit(1)
      }
    }
    return replacedParameter
  }

     def getRecreditFilePath(inputMap: scala.collection.mutable.Map[String, String],sc : SparkContext,spark: SparkSession, feedTypeStr: String): Array[String] = {

    var inputPaths = new ListBuffer[String]()
    var feedType = if(feedTypeStr == null) "Live+7" else feedTypeStr
    var fileType = inputMap("fileType")
    var recreditFilePattern = Util.replaceParamter(inputMap,inputMap("recreditPathPattern"))
    var feedList = populateFeeds(feedType).split(",")
    val conf = sc.hadoopConfiguration
    val fs = org.apache.hadoop.fs.FileSystem.get(conf)

    //var exists = fs.exists(new org.apache.hadoop.fs.Path("/user/fndsstbuat/stb/data/output/STB_Diary/20150420/0/17/multiprocessing/Live+7/21Hrs/"))

    for(feeds <- feedList)
    {
      inputMap += ("feeds" -> feeds)
      var recreditPath = Util.replaceParamter(inputMap,inputMap("recreditPathPattern"))
      var exists = fs.exists(new org.apache.hadoop.fs.Path(recreditPath))
      if(exists)
      inputPaths += recreditPath
    }

    return inputPaths.toArray
  }

  def renamePartFilesToDataFiles(filePath: String,sc : SparkContext,spark: SparkSession) = {
    try{
      val fs = FileSystem.get(sc.hadoopConfiguration);
      var fileList = fs.listStatus(new Path(filePath))
      fileList.foreach(file=> {
        if(file.getPath.toString.contains("part-r-"))
        {
          fs.rename(file.getPath,new Path(file.getPath.toString.replaceAll("part-r-","data-r-")))
        }
        else {
          fs.rename(file.getPath, new Path(file.getPath.toString.replaceAll("part-", "data-r-")))
        }
      })
    }
    catch {
      case e: Exception => {
        logger.error("Exception occured while renaming Files from part-r- to data-r- ")
        e.printStackTrace()
        System.exit(1)
      }
    }
  }

  def populateFeeds(feedType: String): String = {

    if(feedType.equalsIgnoreCase("Live"))
    {
      return "Live"
    }
    else if(feedType.equalsIgnoreCase("Live+0"))
    {
      return "Live,Live+0"
    }
    else if(feedType.equalsIgnoreCase("Live+1"))
    {
      return "Live,Live+0,Live+1"
    }
    else if(feedType.equalsIgnoreCase("Live+2"))
    {
      return "Live,Live+0,Live+1,Live+2"
    }
    else if(feedType.equalsIgnoreCase("Live+3"))
    {
      return "Live,Live+0,Live+1,Live+2,Live+3"
    }
    else if(feedType.equalsIgnoreCase("Live+4"))
    {
      return "Live,Live+0,Live+1,Live+2,Live+3,Live+4"
    }
    else if(feedType.equalsIgnoreCase("Live+5"))
    {
      return "Live,Live+0,Live+1,Live+2,Live+3,Live+4,Live+5"
    }
    else if(feedType.equalsIgnoreCase("Live+6"))
    {
      return "Live,Live+0,Live+1,Live+2,Live+3,Live+4,Live+5,Live+6"
    }
    else if(feedType.equalsIgnoreCase("Live+7"))
    {
      return "Live,Live+0,Live+1,Live+2,Live+3,Live+4,Live+5,Live+6,Live+7"
    }
    else
    {
      logger.error("Exception occured Populating feed for FeedType "+feedType)
      return null
    }
  }
  def addHeaderToDF(df: DataFrame, header: String) : DataFrame =
    {
      var headerSeq = header.split(",").toSeq
      var dfWithHeader = df.toDF(headerSeq: _*)
      return dfWithHeader
    }

    def createTextFile(outputDF: DataFrame, outputPath: String, spark: SparkSession, configuration: Configuration, filename: String, isHeaderNeeded: String) {
    try {

      var fs = FileSystem.get(configuration)
//      val outputRDD = outputDF.rdd.map { x => x.mkString(RPDConstants.PIPE_DELIMITER).substring(0, x.mkString(RPDConstants.PIPE_DELIMITER).length()) }

      println("Deleting temp folder.. " + outputPath + "/temp/")
      if (fs.exists(new Path(outputPath + "/temp/"))) {
        fs.delete(new Path(outputPath + "/temp/"), true)
      }

      var interMediatePath = outputPath.concat("/temp/")
      outputDF.coalesce(1).write.format("com.databricks.spark.csv").mode(SaveMode.Overwrite).option("header", isHeaderNeeded).option("delimiter", "|").save(interMediatePath)

      var outputFile = new Path(outputPath + Path.SEPARATOR + filename)

      if (fs.exists(outputFile)) {
        println("Deleting the existing files from final dir : " + outputFile)
        fs.delete((outputFile), true)
      }
      logger.info("Wrtting from " + interMediatePath + " to " + outputFile)
      FileUtil.copyMerge(fs, new Path(interMediatePath), fs, outputFile, false, configuration, "")

      println("Deleting temp folder.. "+ interMediatePath)
      if (fs.exists(new Path(interMediatePath))) {
        fs.delete(new Path(interMediatePath), true)
      }
      println(outputFile)
    }
    catch {
      case failureException: Exception =>
       println(failureException.getMessage.toString())
       throw new Exception("Failed at : charFileCreation  ", failureException)
    }
  }

  def inputArgsToInputMap(args: Array[String]): mutable.Map[String, String] = {
    var inputMap = scala.collection.mutable.Map("" -> "")
    for (k <- 0 until args.length) {
      var keyPart = args(k).split("=", 2)(0)
      var valPart = args(k).split("=", 2)(1)
      inputMap += (keyPart -> valPart)
    }
    inputMap
  }


    def createTableWithoutPartition(fileName: String, spark: SparkSession, inputMap: mutable.Map[String, String]): Unit = {
    try {
      val sourcePath = inputMap("source")
      logger.info("sourcePath" + sourcePath)
      val basePath = sourcePath.concat("/").concat(fileName).concat("/")
      logger.info("basePath" + basePath)
      val columnList = config.getString(fileName.toLowerCase.concat((".columnList"))).split(",")
      var filterCondition = Util.replaceParamter(inputMap, config.getString(fileName.toLowerCase.concat((".filterCondition"))))


      val df = spark.read.load(basePath).filter(filterCondition).select(columnList.head, columnList.tail: _*)

      df.createOrReplaceTempView(fileName)


    }
    catch {
      case failureException: Exception =>
        println(failureException.getMessage.toString())
        throw new Exception("Failed at : createTable ")
    }
  }

  //***************************************Person-**************************************************
  def getPartitionedPaths(partitionValues: Array[String], partitionColumn: String,basePath: String, pathAfterPartition: String) : List[String] =
  {
    try {
      var pathList = new ListBuffer[String]()

      for(partition <- partitionValues)
      {
        pathList+= basePath + "/" + partitionColumn + "=" + partition + "/" +  pathAfterPartition
      }
      return pathList.toList

    }
    catch {
      case failureException: Exception =>
        println(failureException.getMessage.toString())
        throw new Exception("Failed at : getPartitionedPath  ")
    }
  }

  def createTableAllColumns(fileName: String, partitionValue: Array[String], spark: SparkSession, inputMap: mutable.Map[String, String]): Unit = {
    try {
      val sourcePath = inputMap("source")
      logger.info("sourcePath" + sourcePath)
      val basePath = sourcePath.concat("/").concat(fileName).concat("/")
      logger.info("basePath" + basePath)
      val partitionName = config.getString(fileName.toLowerCase.concat((".partitionName")))
      logger.info("partitionName" + partitionName)
      val partitionedPaths = getPartitionedPaths(partitionValue, partitionName, basePath, "").mkString(",").split(",")
      logger.info("partitionedPaths" + partitionedPaths)
      var filterCondition = Util.replaceParamter(inputMap, config.getString(fileName.toLowerCase.concat((".filterCondition"))))

      if(inputMap("flowType").equalsIgnoreCase("SRA"))
      {
        val df = spark.read.option("basePath", basePath).load(partitionedPaths:_*).filter(filterCondition)
        df.createOrReplaceTempView(fileName+"_SRA")
        logger.info("Counf of " + fileName +":"+df.count)
      }
      else
      {
        val df = spark.read.option("basePath", basePath).load(partitionedPaths:_*).filter(filterCondition)
        df.createOrReplaceTempView(fileName)
        logger.info("Counf of " + fileName +":"+df.count)

      }
    }
    catch {
      case failureException: Exception =>
        println(failureException.getMessage.toString())
        throw new Exception("Failed at : createTable ")
    }
  }

  def createProviderTable(fileName: String, partitionValue: Array[String], spark: SparkSession, inputMap: mutable.Map[String, String],source: String, provider: String): Unit = {
    try {
      val sourcePath = source
      logger.info("sourcePath" + sourcePath)
      val basePath = sourcePath.concat("/").concat(fileName.toLowerCase).concat("/")
      logger.info("basePath" + basePath)
      val partitionName = config.getString(fileName.toLowerCase.concat((".partitionName")))
      logger.info("partitionName" + partitionName)
      val partitionedPaths = getPartitionedPaths(partitionValue, partitionName, basePath, "").mkString(",").split(",")
      logger.info("partitionedPaths" + partitionedPaths)
      var filterCondition = Util.replaceParamter(inputMap, config.getString(fileName.toLowerCase.concat((".filterCondition"))))

      val df = spark.read.option("basePath", basePath).load(partitionedPaths:_*).filter(filterCondition)
      df.createOrReplaceTempView(fileName.toLowerCase+"_"+provider)

    }
    catch {
      case failureException: Exception =>
        println(failureException.getMessage.toString())
        throw new Exception("Failed at : createTable ")
    }
  }

  def createTableWithoutPartitions(fileName: String, spark: SparkSession, inputMap: mutable.Map[String, String]): Unit = {
    try {
      val sourcePath = inputMap("source")
      logger.info("sourcePath" + sourcePath)
      val basePath = sourcePath.concat("/").concat(fileName).concat("/")
      logger.info("basePath" + basePath)
      var filterCondition = Util.replaceParamter(inputMap, config.getString(fileName.toLowerCase.concat((".filterCondition"))))

      if (inputMap("flowType").equalsIgnoreCase("SRA")) {
        val df = spark.read.load(basePath).filter(filterCondition)
        df.createOrReplaceTempView(fileName + "_SRA")
      }
      else {
        val df = spark.read.load(basePath).filter(filterCondition)
        df.createOrReplaceTempView(fileName)
      }
    }

    catch {
      case failureException: Exception =>
        println(failureException.getMessage.toString())
        throw new Exception("Failed at : createTable ")
    }
  }
}
