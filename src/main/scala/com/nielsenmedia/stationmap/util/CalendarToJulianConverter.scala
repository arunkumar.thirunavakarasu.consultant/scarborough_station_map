package com.nielsenmedia.stationmap.util

import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.util.{Calendar, Date}

object CalendarToJulianConverter {


  def convertDateToJulianFormat(datadate:String):Double={

    /*var year = datadate.get(Calendar.YEAR)
    var month = datadate.get(Calendar.MONTH)+1
    var day = datadate.get(Calendar.DAY_OF_MONTH)
    var hour = datadate.get(Calendar.HOUR_OF_DAY)
    var minute = datadate.get(Calendar.MINUTE)
    var second = datadate.get(Calendar.SECOND)*/

    /*var year = datadate.getYear()
    var month = datadate.getMonth()+1
    var day = datadate.getMonth()
    var hour = datadate.getHours()
    var minute = datadate.getMinutes()
    var second = datadate.getSeconds()*/

    var myFormat = new SimpleDateFormat("yyyy-MM-dd")
    var date = myFormat.parse(datadate)
    //var formattedDate = myFormat.format(date)
    var year = date.getYear()
    var month = date.getMonth()+1
    var day = date.getMonth()
    var hour = date.getHours()
    var minute = date.getMinutes()
    var second = date.getSeconds()

    var extra = (100.0 * year) + month - 190002.5

    return (367.0 * year) -
      (Math.floor(7.0 * (year + Math.floor((month + 9.0) / 12.0)) / 4.0)) +
      Math.floor((275.0 * month) / 9.0) +
      day + ((hour + ((minute + (second / 60.0)) / 60.0)) / 24.0) +
      1721013.5 - ((0.5 * extra) / Math.abs(extra)) + 0.5

  }

  def convertToJulian(datadate:String):Int={

    var myFormat = new SimpleDateFormat("yyyy-MM-dd")
    var date = myFormat.parse(datadate)
    var calendar = Calendar.getInstance()
    calendar.setTime(date)
    var year = calendar.get(Calendar.YEAR)
    var syear = myFormat.format("%04d",year).substring(2)
    var century = Integer.parseInt(String.valueOf(((year / 100)+1)).substring(1))
    var julian = Integer.parseInt(myFormat.format("%d%s%03d",century,syear,calendar.get(Calendar.DAY_OF_YEAR)))
    return julian

  }


    def convertingToJulian(datadate:String):Unit={

    var fromUser = new SimpleDateFormat("yyyy-MM-dd")
    var myFormat = new SimpleDateFormat("yyyyMMdd")
    var reformattedStr = myFormat.format(fromUser.parse(datadate))

    var year = Integer.parseInt(reformattedStr.substring(0, 3))
    var month = Integer.parseInt(reformattedStr.substring(4, 5))
    var day = Integer.parseInt(reformattedStr.substring(6,7))

    var date = Integer.parseInt(reformattedStr)
    var y2 = date / 1000
    var m2 = (date % 1000) / 100
    var d2 = date % 10000
  }


   def getJulianDate(normalDate:Date ):String={

    var date = new SimpleDateFormat("yyyyMMdd").format(normalDate)
    var simpleDateFormat = new SimpleDateFormat("yyyyMMdd")
    var newDate = simpleDateFormat.parse(date)
    var cal = Calendar.getInstance()
    cal.setTime(newDate)
    var sdf = new SimpleDateFormat("yy")
    var formattedDate = sdf.format(newDate)
    var s = cal.get(6)
    return formattedDate+CalendarToJulianConverter.prefixLeadingZeroesFor(s, 3)
  }

  def prefixLeadingZeroesFor(number:Int , maximumLength:Int ):String={

    var numberString = String.valueOf(number)
    var paddingSize = maximumLength - numberString.length()
    var zeroes = ""
    var i=0
    for (i <- 0 until paddingSize)
      zeroes = zeroes + "0"
    return zeroes + numberString
  }



  def getCurrenttimeStamp():String={

    var timestamp = new Timestamp(System.currentTimeMillis())
    return timestamp.toString()
  }

  def getCurrentTime():String={

    //var timeInMillis = System.currentTimeMillis()
    var cal = Calendar.getInstance()
    //cal.setTimeInMillis(timeInMillis)
    var sdf = new SimpleDateFormat("hhmm")
    return sdf.format(cal.getTime()).toString()
  }

}
