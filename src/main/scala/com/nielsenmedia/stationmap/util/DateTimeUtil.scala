
package com.nielsenmedia.stationmap.util

import java.io.IOException
import java.text.{ParseException, _}
import java.util._

import org.apache.commons.lang.time._
import com.nielsenmedia.stationmap.util.DateTimeUtil.{FORMAT_MMDDYYYY, FORMAT_YYYYMMDD, formatDate, parseDate}

object DateTimeUtil {


  val FORMAT_YYYYMMDD = "yyyyMMdd"
  val FORMAT_YYYYMMDD_WITH_DASH = "yyyy-MM-dd"
  val FORMAT_JULIAN_DATE = "1yyDDD"
  val FORMAT_MMDDYYYY = "MM/dd/yyyy"

  @throws(classOf[ParseException])
  def getPreviousDate(dateStr: String): String = {
    return addDaysTodate(dateStr, -1)
  }

  @throws(classOf[ParseException])
  def getStartDate(dateStr: String, days: Int): String = {
    return addDaysTodate(dateStr, -days)
  }

  @throws(classOf[ParseException])
  def getformatedDate(dateStr: String): String = {
    var myFormat = new SimpleDateFormat("yyyyMMdd")
    var date = myFormat.parse(dateStr)
    return myFormat.format(date)
  }

  /*
   * Returns date in format MM/dd/yyyy as String
   */
  @throws(classOf[ParseException])
  def getFormatedDate(dateStr: String): String = {
    val myFormat = new SimpleDateFormat("yyyyMMdd")
    val requiredFormat = new SimpleDateFormat("MM/dd/yyyy")
    val date = myFormat.parse(dateStr)
    return requiredFormat.format(date)
  }

  @throws(classOf[ParseException])
  def getTheNextDate(dateStr: String): String = {
    return addDaysTodate(dateStr, 1)
  }

  @throws(classOf[ParseException])
  def addDaysTodate(dateStr: String, days: Int): String = {
    var myFormat = new SimpleDateFormat("yyyyMMdd")
    var date = myFormat.parse(dateStr)
    var newDate = DateUtils.addDays(date, days)
    return myFormat.format(newDate)
  }

  @throws(classOf[ParseException])
  @throws(classOf[IOException])
  def getNextDate(dateStr: String): String = {
    var myFormat = new SimpleDateFormat("yyyyMMdd")
    var date = myFormat.parse(dateStr)

    /*
         * var nextDate = DateUtils.addDays(date, 1)
        return myFormat.format(nextDate).toString()*/

    /*Another Way for getting the next day*/
    var calendar = Calendar.getInstance()
    calendar.setTime(date)
    calendar.add(Calendar.DATE, 1)
    return myFormat.format(calendar.getTime).toString()

  }
  @throws(classOf[ParseException])
  def toMMDDYYYY(dateStr: String): String = {
    formatDate(FORMAT_MMDDYYYY, parseDate(FORMAT_YYYYMMDD, dateStr))
  }
  private def formatDate(format: String, date: Date): String = {
    new SimpleDateFormat(format).format(date)
  }
  private def parseDate(format: String, dateStr: String): Date = {
    new SimpleDateFormat(format).parse(dateStr)
  }

}
