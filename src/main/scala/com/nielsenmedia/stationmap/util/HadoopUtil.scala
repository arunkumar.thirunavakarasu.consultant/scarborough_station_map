package com.nielsenmedia.stationmap.util

import java.io.{File, IOException}
import java.util.{ArrayList, TreeSet}

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, GlobFilter, Path, _}
import org.apache.log4j._

import scala.collection.JavaConversions._

object HadoopUtil {

  val logger = Logger.getLogger(getClass().getName)
  var configuration = new Configuration()
  var fs = FileSystem.get(configuration)

  @throws(classOf[IOException])
  def listFiles(srcPath: String, pathPattern: String, recursive: Boolean, configuration: Configuration): ArrayList[Path] = {

    //var directory = new Path(srcPath)
    var paths = new ArrayList[Path]
    var outputFilter = new GlobFilter("[^_]*"); // Filtering only the output files
    var listStatus = fs.globStatus(new Path(srcPath + File.separator + pathPattern), outputFilter);
    for (fstat <- listStatus) {
      if (!fstat.isDirectory()) {
        paths.add(fstat.getPath());
      }
    }

    if (recursive) {
      listStatus = fs.globStatus(new Path(srcPath + File.separator + "*"));
      for (fstat <- listStatus) {
        if (fstat.isDirectory()) {
          readSubDirectory(fstat, pathPattern, outputFilter, fs, paths);
        }
      }
    }

    return paths
  }

  @throws(classOf[IOException])
  def readSubDirectory(fileStatus: FileStatus, pathPattern: String, outputFilter: GlobFilter, fs: FileSystem, paths: ArrayList[Path]): Unit = {
    var subDir = fileStatus.getPath.toString()
    var listStatus = fs.globStatus(new Path(subDir + File.separator + pathPattern), outputFilter)

    for (fstat <- listStatus) {
      if (!fstat.isDirectory()) {
        paths.add(fstat.getPath)
      }
    }

    listStatus = fs.globStatus(new Path(subDir + File.separator + "*"));
    if (listStatus.length > 0) {
      for (fstat <- listStatus) {
        if (fstat.isDirectory()) {
          readSubDirectory(fstat, pathPattern, outputFilter, fs, paths);
        }
      }
    }

  }

  @throws(classOf[IOException])
  def deleteOutputDirectory(srcPath: Path, sourceFilePaths: ArrayList[Path], trgtPath: Path, configuration: Configuration): Unit = {

    var outputDirs = new TreeSet[Path]

    // Get the unique output dirs to delete
    for (srcFilePath <- sourceFilePaths) {
      var trgtFilename = srcFilePath.toString().replace(srcPath.toString(), trgtPath.toString());
      var parentPath = new Path(trgtFilename).getParent();
      outputDirs.add(parentPath);
    }

    var fs = FileSystem.get(configuration)

    // Delete the output dirs
    for (outputDir <- outputDirs) {
      fs.delete(outputDir, true);
      if (logger.isDebugEnabled()) {
        logger.debug("Deleted output dir: " + outputDir);
      }
    }
  }

  /**
    * Deletes all the files inside target directory
    * @param filePath
    */
  def deleteAllFilesIfPathExists(filePath: String): Unit = {
    var fs = FileSystem.get(new Configuration())
    val path = new Path(filePath)
    if (fs.exists(path)) {
      logger.info("Deleting the existing files from final dir : " + filePath)
      fs.delete(path, true)
    }
    logger.info("Write data frame into file: " + filePath)
  }

  def copyFile(fs: FileSystem, src: Path, dstDir: Path, deleteSource: Boolean, overwrite: Boolean, conf: Configuration) = {
    val files = fs.globStatus(src)
    val csvFile = files(0).getPath
    println("File is cpying: " + csvFile + " to " +dstDir)
    FileUtil.copy(fs, csvFile, fs, dstDir, deleteSource, overwrite, conf);
  }

  /**
    * Copy file from intermediatePath to outputPath
    * @param intermediatePath
    * @param outputPath
    */
  def copyFile(intermediatePath: String, outputPath: String): Unit = {
    val configuration = new Configuration()
    var fs = FileSystem.get(new Configuration())
    FileUtil.copyMerge(fs, new Path(intermediatePath), fs, new Path(outputPath), false, configuration, "")
    logger.info(s"File copied from $intermediatePath into $outputPath")
  }

  /**
    * Copy files from intermediatePath to outputPath and merge them into a single file
    * @param intermediatePath
    * @param outputPath
    */
  def mergeFile(intermediatePath: String, outputPath: String): Unit = {
    val configuration = new Configuration()
    var fs = FileSystem.get(new Configuration())
    FileUtil.copyMerge(fs, new Path(intermediatePath), fs, new Path(outputPath), false, configuration, "")
    logger.info(s"File copied from $intermediatePath into $outputPath")
  }
}
