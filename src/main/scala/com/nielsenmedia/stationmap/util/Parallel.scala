package com.nielsenmedia.stationmap.util

import scala.concurrent.duration.Duration
import scala.concurrent.{Await,ExecutionContext, Future}

object Parallel {
  implicit val ec :ExecutionContext = ExecutionContext.global
  def awaitAll(timeout: Duration, jobs:() => Unit*): Unit ={
    val futures = jobs.map(job => Future { job () })
    Await.result(Future.sequence(futures), timeout)
  }
}
