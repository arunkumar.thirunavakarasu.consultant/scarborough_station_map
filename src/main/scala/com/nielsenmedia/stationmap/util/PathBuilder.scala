package com.nielsenmedia.stationmap.util

import scala.util.Try

object PathBuilder {

  object Segment {
    final val MARKET_TYPE     = 'market_type
    final val DATE            = 'date
    final val DCA             = 'dca
    final val SSCM            = 'sscm
    final val FEED_TYPE       = 'feed_type
    final val FILE_TYPE       = 'file_type
    final val PROJECTION_TYPE = 'projection_type
    final val BUCKET          = 'bucket
    final val FILENAME        = 'filename
  }

  implicit class PathOps(path: String) {

    private def segmentRegexFor(key: Symbol) =
      s"\\{\\{(?i)${key.name}\\}\\}"

    private val segmentRegex =
      ".*\\{\\{(.*)\\}\\}.*".r

    def hasSegment(key: Symbol): Boolean =
      path.matches(s".*${segmentRegexFor(key)}.*")

    def segment(key: Symbol, value: => String): String =
      Try(value).toOption.map(
        path.replaceAll(segmentRegexFor(key), _)
      ).getOrElse(path)

    def assertNoUninitializedSegments: String = path match {
      case segmentRegex(key) =>
        throw new RuntimeException(
          s"Unrecognized path segment '$key' in path $path")
      case _ => path
    }

    def toGzipped: String = s"$path.gz"
  }
}

