package com.nielsenmedia.stationmap.util

import java.util.concurrent._

/**
  * Contains utils for async processing.
  *
  * Please, avoid here any code contains business-logic, this must be abstract.
  */
object ConcurrentUtils {
  private lazy val DEFAULT_THREAD_POOL_SIZE = 2
  private lazy val DEFAULT_TIMEOUT = (1, TimeUnit.HOURS)

  /**
    * Tasks that run asynchronously can be represented as a list if Futures.
    * This method allows to check that all the tasks succeed.
    *
    * @param futures - futures to check
    */
  def monitorFutures(futures: Future[_]*): Unit = {
    for (future <- futures) {
      try {
        future.get
      } catch {
        case e: Exception => throw new RuntimeException(e)
      }
    }
  }

  /**
    * Run multiple tasks asynchronously
    *
    * @param executorService - instance of ExecutorService with specified Number of threads
    * @param runnables - tasks to run asynchronously
    */
  def runParallel(executorService: ExecutorService, runnables: Runnable*): Unit = {
    val futures = runnables.map(executorService.submit(_))
    executorService.shutdown()
    monitorFutures(futures:_*)
    executorService.awaitTermination(DEFAULT_TIMEOUT._1, DEFAULT_TIMEOUT._2)
  }

  /**
    * Overloaded runParallel with specified number of threads
    *
    * @param nThreads - number of threads
    * @param runnables - tasks to run asynchronously
    */
  def runParallel(nThreads: Int, runnables: Runnable*): Unit = {
    runParallel(Executors.newFixedThreadPool(nThreads), runnables:_*)
  }

  /**
    * Overloaded runParallel with default ExecutorService
    *
    * @param runnables - tasks to run asynchronously
    */
  def runParallel(runnables: Runnable*): Unit = {
    runParallel(Executors.newFixedThreadPool(DEFAULT_THREAD_POOL_SIZE), runnables:_*)
  }

}
