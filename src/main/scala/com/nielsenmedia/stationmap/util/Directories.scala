package com.nielsenmedia.stationmap.util

import java.io.File

object Directories {
  def path(dirs: String*): String = {
    dirs.mkString(File.separator)
  }
}
