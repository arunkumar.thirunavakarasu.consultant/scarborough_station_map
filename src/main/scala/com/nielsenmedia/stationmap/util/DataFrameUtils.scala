package com.nielsenmedia.stationmap.util

import org.apache.hadoop.fs.{FileUtil, Path}
import org.apache.log4j.Logger
import com.nielsenmedia.stationmap.util.PathBuilder._
import java.util.UUID
import org.apache.spark.sql.{DataFrame, Row, SaveMode, SparkSession}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.types.{StringType, StructField, StructType}

object DataFrameUtils {
  val LOG = Logger.getLogger(this.getClass.getSimpleName)

  def sortCsvColumns(cols: String*)(df: DataFrame): DataFrame = {
    import org.apache.spark.sql.functions._

    val sortCsvColumn = udf((list: String) =>
      list.split(",").sorted.mkString(","))

    cols.foldLeft(df) { (df, colName) =>
      df.withColumn(colName, sortCsvColumn(col(colName)))
    }
  }

  /**
    * Writes the Dataframe into a single output file or partition files. Currently, assumes .csv output.
    * TODO This is a temporary home until we have a storage client type (.csv, .parquet) abstraction implemented.
    * TODO This duplicates some stuff in com.nielsenmedia.stationmap.extracts.FileUtil
    *
    * @param df
    * @param outputFilePath The path to the final output file.
    * @param merge If true, path point to the single final output file. Otherwise it point to the folder for the partition files.
    * @param spark
    */
  def writeDataFrame(df: DataFrame, outputFilePath: Path, merge: Boolean = true, writeCrc:Boolean = true, header: Boolean = false)
                    (implicit spark: SparkSession): Unit = {

    if(merge) {
      LOG.info(s"Writing Output File: $outputFilePath")
      val tmpFilePath = new Path(outputFilePath.toString + ".tmp")
      val fs = outputFilePath.getFileSystem(spark.sparkContext.hadoopConfiguration)

      // TODO, separate configs for the temp file path vs the final output path, so we can leverage hdfs tmp even when writing to s3
      fs.delete(tmpFilePath, true)
      fs.delete(outputFilePath, false)

      if (header) // TODO - find a better solution for this. (Need to coalesce so we don't end up with multiple headers)
        df.coalesce(1).write.option("sep", "|").option("header", "true").csv(tmpFilePath.toString)
      else
        df.write.option("sep", "|").option("header", "false").csv(tmpFilePath.toString)

      fs.setWriteChecksum(writeCrc)

      // Move to final single file output
      FileUtil.copyMerge(fs, tmpFilePath, fs, outputFilePath, true, spark.sparkContext.hadoopConfiguration, null)
    } else {
      LOG.info(s"Writing Output Partition Files to: $outputFilePath")
      val fs = outputFilePath.getFileSystem(spark.sparkContext.hadoopConfiguration)
      fs.setWriteChecksum(writeCrc)

      // Write partition files, overwrite will delete first
      df.write.option("sep", "|").option("header", "false").mode(SaveMode.Overwrite).csv(outputFilePath.toString)
    }
  }

  /**
    * Returns a single dataframe from the passed in paths. Applies column headers if provided
    * TODO This is a temporary home until we have a storage client type (.csv, .parquet) abstraction implemented.
    *
    * @param paths
    * @param columnNames
    * @param spark
    * @return
    */
  def getMergedDataFrame(paths: Array[Path], columnNames: Array[String] = Array[String]())(implicit spark: SparkSession): DataFrame = {
    val pathList = paths.map(p => p.toString)
    var df = spark.read.option("inferSchema", "false").option("header", "false").option("delimiter", "|").csv(pathList:_*)

    // Rename the columns based on the passed in column names
    for ((colName, idx) <- columnNames.view.zipWithIndex) {
      if (idx < df.schema.fieldNames.length) {
        df = df.withColumnRenamed(df.schema.fieldNames(idx), colName)
      }
    }

    df
  }

  //  Write Data Frame with Compression
  def writeDataFrames(df: DataFrame, outputFilePath: Path, merge: Boolean = true,
                      writeCrc: Boolean = true, header: Boolean = false, compression: Boolean = false)
                     (implicit spark: SparkSession): Unit = {

    import spark.implicits._

    val finalFilePath = if (compression) {
      new Path(s"${outputFilePath.toString}.gz")
    }
    else {
      outputFilePath
    }

    val fs = finalFilePath.getFileSystem(spark.sparkContext.hadoopConfiguration)
    fs.delete(finalFilePath, true)
    fs.setWriteChecksum(writeCrc)
    LOG.info(s"Writing Output File: $finalFilePath")
    println("Writing Output File - " + finalFilePath)
    val actualOutputPath =
      if (merge) new Path(s"${UUID.randomUUID()}.tmp")
      else finalFilePath

    val headers = df.schema.fields.map(_.name).mkString("|")
    val dfAsString = df.na.fill("").map(_.mkString("|")).toDF(headers)

    dfAsString
      .write
      .option("sep", "\u0000")
      .option("header", header && !merge)
      .option("quote", "\u0000")
      .option("compression", if (compression && !merge) "gzip" else "none")
      .option("ignoreLeadingWhiteSpace", "false")
      .option("ignoreTrailingWhiteSpace", "false")
      .mode(SaveMode.Overwrite)
      .csv(actualOutputPath.toString)

    if (merge) {
      val headersParam = if (header) Some(headers) else None
    }
  }

  def writeDataFrameWithCountFile(df: DataFrame, outputFilePath: Path, outputCountFilePath: Path, merge: Boolean = true,
                                  writeCrc: Boolean = true, header: Boolean = false, compression: Boolean = false)
                                 (implicit spark: SparkSession): Unit = {

    val (finalFileName, finalCountFilePath) = if (compression) {
      (outputFilePath.getName.toGzipped, outputCountFilePath.toString.toGzipped)

    }
    else {
      (outputFilePath.getName, outputCountFilePath)
    }

    writeDataFrames(df.cache(), outputFilePath, merge, writeCrc, header, compression)
    val fileNameAndRowCount = s"$finalFileName,${df.count}\n"
  }

  def createHeaderDF(spark: SparkSession, columnNames: Array[String]): DataFrame = {
    val headerRow: Row = Row(columnNames: _*)
    val headerRDD: RDD[Row] = spark.sparkContext.makeRDD(List(headerRow))
    val schema = StructType(columnNames.map { colName => StructField(colName, StringType) })
    spark.sqlContext.createDataFrame(headerRDD, schema)
  }

}
