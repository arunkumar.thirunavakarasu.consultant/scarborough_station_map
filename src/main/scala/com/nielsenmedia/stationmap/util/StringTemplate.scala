package com.nielsenmedia.stationmap.util

/**
  * Lightweight string templating. Substitutes pattern {{name}} with corresponding value.
  * This class is not Thread Safe at this time.
  * @param template
  */
case class StringTemplate(template: String) {
  private var updatedString = template

  /**
    * Substitute a template parameter. Ignores missing parameter if not in the template.
    * @param name
    * @param value
    * @return
    */
  def set(name: String, value: String): StringTemplate = {
    updatedString = updatedString.replaceAllLiterally(s"{{$name}}", value)
    this
  }

  /**
    * Allow the template builder to be reused
    */
  def reset: StringTemplate = {updatedString = template; this}

  /**
    * @return The updated String
    */
  def build: String = {
    updatedString
  }
}

