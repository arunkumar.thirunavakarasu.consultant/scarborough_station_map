package com.nielsenmedia.stationmap.constants

import com.nielsenmedia.stationmap.config.Configurable.config
import com.nielsenmedia.stationmap.util.Encryption

object Constants {

  //S3 buckets
  val diaryS3Path: String = config.getString("s3.path.diary")
}
