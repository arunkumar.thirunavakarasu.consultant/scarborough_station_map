package com.nielsenmedia.stationmap.scarborough

import com.nielsenmedia.stationmap.config.Configurable.config
import com.nielsenmedia.stationmap.util.Encryption

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.{SQLContext, SaveMode, SparkSession}
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.log4j.{Level, Logger}
import org.apache.spark
import java.time.LocalDateTime

object StationMap {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf()
      .setMaster("local[*]")
    val LOG: Logger = Logger.getLogger(getClass)
    Logger.getLogger("org").setLevel(Level.WARN)


    val workInputDir: String = "file:///C:/Users/thirar01/Desktop/Scarborough/"
    val workOutputDir: String = "file:///C:/Users/thirar01/Desktop/Scarborough/output/"

    println("Station Mapping Job Starting : "+LocalDateTime.now())

    val spark = SparkSession
      .builder()
      .appName("SparkApp")
      .config(conf)
      .getOrCreate()

    val temp_s42_df = spark.read.option("inferSchema", "true").format("com.databricks.spark.csv").option("delimiter", "|").option("header", "true").load(workInputDir+"s42.txt")
    temp_s42_df.createOrReplaceTempView("temp_s42")

    val temp_scarborough_market_code_map_df = spark.read.option("inferSchema", "true").format("com.databricks.spark.csv").option("delimiter", "|").option("header", "true").load(workInputDir+"scarboroughMarketMap.txt")
    temp_scarborough_market_code_map_df.createOrReplaceTempView("temp_scarborough_market_code_map")

    val src_scarborough_station_map_df = spark.read.option("inferSchema", "true").format("com.databricks.spark.csv").option("delimiter", "|").option("header", "true").load(workInputDir+"scarboroughStationMap.txt")
    src_scarborough_station_map_df.createOrReplaceTempView("src_scarborough_station_map")

    val stg1_scarborough_station_map_df = spark.sql( "select *, case when CALLLETTER in ( '', 's)' ) then '' else substring_index(upper(substring(Description," +
      "instr(Description, '(')+1,(instr(Description, ')') - instr(Description, '('))-1)),'CHANNEL ',-1) " +
      "end as channelNumber from src_scarborough_station_map")
    stg1_scarborough_station_map_df.createOrReplaceTempView("stg1_scarborough_station_map")

    //Place the Parquet file in inputDir for tv_media_credit for Local Testing.
    //us-east-1-nlsn-w-tam-ltamingestion-gtam-prod-03/data/panel_ingestion/external/TV_MEDIA_CREDIT/
    val tv_media_credit_df = spark.read.load(workInputDir+"TV_MEDIA_CREDIT/")
    tv_media_credit_df.createOrReplaceTempView("tv_media_credit")

   val stg2_scarborough_station_map_df = spark.sql("select  t.PRIMARYSTUDYID," +
      "   t.DESCRIPTION," +
      "   t.SCARBNETWORKKEY, " +
      "   case when t.SCARBKEY is null then '' else t.SCARBKEY end as SCARBKEY," +
      "   t.VALUE, t.NETWORKNAME, " +
      "   case when t.CALLLETTER  = 's)' then '' else t.CALLLETTER end as CALLLETTER," +
      "   channelNumber," +
      "   case when t.STATIONID is null then '' else t.STATIONID end as STATIONID," +
      "   case when t.STATIONNAME is null then '' else t.STATIONNAME end as STATIONNAME," +
      "   nvl(sd.METERED_TV_CREDIT_CALL_LETTERS, '') as DD_CALL_LETTERS, " +
      "   case when sd.METERED_TV_CREDIT_STATION_CODE is null then '' else  sd.METERED_TV_CREDIT_STATION_CODE end as DD_STATION_CODE, " +
      "   case when sd.DESIGNATED_MARKET_AREA_OF_ORIGIN_ID is null then '' else  sd.DESIGNATED_MARKET_AREA_OF_ORIGIN_ID end as DD_STATION_ORIGIN, " +
      "   nvl(sd.FIRST_NSI_PBL_AFFIL_S_NAME, '') as DD_FIRST_NSI_PBL_AFFIL_S_NAME," +
      "   nvl(sd.FIRST_NSI_PBL_AFFIL_DESC, '') as DD_FIRST_NSI_PBL_AFFIL_DESC, " +
      "   nvl(sd.DISTRIBUTION_SOURCE_NAME, '') as DD_DISTRIBUTION_SOURCE_NAME," +
      "   CASE WHEN t.STATIONID = '' OR t.STATIONID is null OR sd.METERED_TV_CREDIT_STATION_CODE is null   " +
      "         THEN 'No'" +
      "         WHEN trim(upper(FIRST_NSI_PBL_AFFIL_DESC)) = trim(upper(t.NETWORKNAME))" +
      "         THEN 'Yes'" +
      "         WHEN trim(upper(FIRST_NSI_PBL_AFFIL_DESC)) like concat('%',trim(upper(t.NETWORKNAME)),'%')    " +
      "         THEN 'Yes'    " +
      "         WHEN trim(upper(t.NETWORKNAME)) like concat('%',trim(upper(FIRST_NSI_PBL_AFFIL_DESC)),'%')  " +
      "         THEN 'Yes'" +
      "         WHEN trim(upper(t.NETWORKNAME)) like concat('%',trim(upper(sd.FIRST_NSI_PBL_AFFIL_S_NAME)),'%')  " +
      "         THEN 'Yes'    " +
      "         else 'No'  " +
      "         END as network_match_flag," +
      "   CASE WHEN sd.DISTRIBUTION_SOURCE_NAME like concat('%',t.callLetter,'%',t.channelNumber,'%')    " +
      "         THEN 'Yes'" +
      "         ELSE 'No'    " +
      "         END as channel_num_match_flag    " +
      "   from    stg1_scarborough_station_map t" +
      "   left outer join tv_media_credit  sd" +
      "       on '2020-02-15' between sd.EFFECTIVE_START_DATE and sd.EFFECTIVE_END_DATE" +
      "       and sd.RELEASED_FOR_PROCESSING_FLAG = 'Y'" +
      "       and sd.METERED_TV_CREDIT_CALL_LETTERS = t.callLetter")
    stg2_scarborough_station_map_df.createOrReplaceTempView("stg2_scarborough_station_map")


    val stg3_scarborough_station_map_df = spark.sql("select " +
      "row_number() over(partition by null order by mmap.MARKET_CODE, SCARBNETWORKKEY, VALUE) as rec_key," +
      "mmap.MARKET_NAME," +
      "mmap.MARKET_TYPE," +
      "mmap.MARKET_CODE," +
      "mmap.DMA_ID," +
      "smap.*,nvl(sdmap.MARKET_NAME, '') as DD_STATION_ORIGIN_MARKET_NAME," +
      "case when smap.DD_STATION_ORIGIN = '' " +
      "     then ''" +
      "     when smap.DD_STATION_ORIGIN = mmap.dma_id " +
      "     then 'Yes'" +
      "     else 'No'" +
      "     end as in_market_station_flag," +
      " case when smap.NETWORK_MATCH_FLAG = 'No' and smap.CHANNELNUMBER in ('')" +
      "     then 3" +
      "     when channel_num_match_flag = 'No' or ( smap.NETWORK_MATCH_FLAG = 'No' and smap.CHANNELNUMBER not in ('') )" +
      "     then 2" +
      "     else 1" +
      "     end as mapping_stage " +
      " from stg2_scarborough_station_map smap " +
      " left outer join  temp_scarborough_market_code_map mmap" +
      "     on smap.PRIMARYSTUDYID = mmap.studyid" +
      "     or smap.PRIMARYSTUDYID like concat('%',MARKET_CODE,'%') " +
      " left outer join     temp_scarborough_market_code_map sdmap" +
      "     on sdmap.dma_id = smap.DD_STATION_ORIGIN")

    stg3_scarborough_station_map_df.createOrReplaceTempView("stg3_scarborough_station_map")

    spark.sql("select mapping_stage, count(*) from stg3_scarborough_station_map group by 1 order by mapping_stage").show(100,false)

    val iteration_1_df = spark.sql("SELECT " +
      "REC_KEY, MARKET_NAME, MARKET_TYPE, MARKET_CODE, DMA_ID, PRIMARYSTUDYID, DESCRIPTION," +
      "SCARBNETWORKKEY, SCARBKEY, VALUE, NETWORKNAME, CALLLETTER, CHANNELNUMBER,STATIONID," +
      "STATIONNAME,DD_STATION_CODE,DD_CALL_LETTERS, DD_FIRST_NSI_PBL_AFFIL_S_NAME, DD_FIRST_NSI_PBL_AFFIL_DESC," +
      "DD_DISTRIBUTION_SOURCE_NAME, DESCRIPTION AS DD_DESCRIPTION,NETWORK_MATCH_FLAG, DD_STATION_ORIGIN,DD_STATION_ORIGIN_MARKET_NAME," +
      "IN_MARKET_STATION_FLAG from stg3_scarborough_station_map where mapping_stage = 1 order by rec_key")

    println("Writing Iteration 1 Mapped file "+LocalDateTime.now())
    iteration_1_df.coalesce(1).write.mode(SaveMode.Overwrite).format("com.databricks.spark.csv").option("header", "true").save(workOutputDir+"Iteration_1.xls")

    val iteration_2_df = spark.sql("SELECT " +
      "REC_KEY, MARKET_NAME, MARKET_TYPE, MARKET_CODE, DMA_ID, PRIMARYSTUDYID, DESCRIPTION," +
      "SCARBNETWORKKEY, SCARBKEY, VALUE, NETWORKNAME, CALLLETTER, CHANNELNUMBER,STATIONID," +
      "STATIONNAME,DD_STATION_CODE,DD_CALL_LETTERS, DD_FIRST_NSI_PBL_AFFIL_S_NAME, DD_FIRST_NSI_PBL_AFFIL_DESC," +
      "DD_DISTRIBUTION_SOURCE_NAME, DESCRIPTION AS DD_DESCRIPTION,NETWORK_MATCH_FLAG, DD_STATION_ORIGIN,DD_STATION_ORIGIN_MARKET_NAME," +
      "IN_MARKET_STATION_FLAG from stg3_scarborough_station_map where mapping_stage = 2 order by rec_key")

    println("Writing Iteration 2 Mapped file "+LocalDateTime.now())
    iteration_2_df.coalesce(1).write.mode(SaveMode.Overwrite).format("com.databricks.spark.csv").option("header", "true").save(workOutputDir+"Iteration_2.xls")


    val stg4_scarborough_station_map_df = spark.sql("select distinct t.*," +
      " nvl(ssd.STATIONID, '') as P2_STATIONID," +
      " nvl(ssd.STATIONNAME, '') as P2_STATIONNAME," +
      " nvl(sd.METERED_TV_CREDIT_CALL_LETTERS, '') as P2_DD_CALL_LETTERS," +
      " case when sd.METERED_TV_CREDIT_STATION_CODE is null then '' else sd.METERED_TV_CREDIT_STATION_CODE end as P2_DD_STATION_CODE," +
      " nvl(sd.FIRST_NSI_PBL_AFFIL_S_NAME, '') as P2_FIRST_NSI_PBL_AFFIL_S_NAME," +
      " nvl(sd.FIRST_NSI_PBL_AFFIL_DESC, '') as P2_DD_FIRST_NSI_PBL_AFFIL_DESC," +
      " nvl(sd.DISTRIBUTION_SOURCE_NAME, '') as P2_DD_DISTRIBUTION_SOURCE_NAME," +
      " case " +
      "   when t.STATIONID = '' OR sd.METERED_TV_CREDIT_STATION_CODE is null " +
      "   then 'No' " +
      "   when trim(upper(FIRST_NSI_PBL_AFFIL_DESC)) = trim(upper(t.NETWORKNAME)) " +
      "   then 'Yes'" +
      "   when trim(upper(FIRST_NSI_PBL_AFFIL_DESC)) like  concat('%',trim(upper(t.NETWORKNAME)),'%')" +
      "   then 'Yes'" +
      "   when trim(upper(t.NETWORKNAME)) like  concat('%',trim(upper(FIRST_NSI_PBL_AFFIL_DESC)),'%')" +
      "   then 'Yes'" +
      "   when trim(upper(t.NETWORKNAME)) like  concat('%',trim(upper(sd.FIRST_NSI_PBL_AFFIL_S_NAME)),'%')" +
      "   then 'Yes'" +
      "   else 'No'" +
      "   end as P2_NETWORK_MATCH_FLAG " +
      " from stg3_scarborough_station_map t" +
      " left outer join tv_media_credit sd" +
      " on '2020-02-15' between sd.EFFECTIVE_START_DATE and sd.EFFECTIVE_END_DATE " +
      " and sd.RELEASED_FOR_PROCESSING_FLAG = 'Y' " +
      " and sd.DISTRIBUTION_SOURCE_NAME like concat('%',t.callLetter,'%',t.channelNumber,'%')" +
      " and sd.DESIGNATED_MARKET_AREA_OF_ORIGIN_ID = t.DMA_ID" +
      " left outer join temp_s42 ssd" +
      " on ssd.STATIONNAME = sd.METERED_TV_CREDIT_CALL_LETTERS" +
      " and ssd.StudyID = t.PRIMARYSTUDYID " +
      " where mapping_stage = 2 ")

    stg4_scarborough_station_map_df.createOrReplaceTempView("stg4_scarborough_station_map")

    val report_df = spark.sql("select t.REC_KEY, MARKET_NAME, MARKET_TYPE, MARKET_CODE," +
      " DMA_ID, PRIMARYSTUDYID, DESCRIPTION, SCARBNETWORKKEY, SCARBKEY," +
      " VALUE, NETWORKNAME, CALLLETTER, CHANNELNUMBER, STATIONID," +
      " STATIONNAME, DD_STATION_CODE, DD_CALL_LETTERS, DD_FIRST_NSI_PBL_AFFIL_S_NAME, " +
      " DD_FIRST_NSI_PBL_AFFIL_DESC, DD_DISTRIBUTION_SOURCE_NAME, DESCRIPTION as DD_DESCRIPTION, NETWORK_MATCH_FLAG," +
      " DD_STATION_ORIGIN, DD_STATION_ORIGIN_MARKET_NAME, IN_MARKET_STATION_FLAG, P2_STATIONID, P2_STATIONNAME," +
      " P2_DD_STATION_CODE, P2_DD_CALL_LETTERS, P2_FIRST_NSI_PBL_AFFIL_S_NAME, P2_DD_FIRST_NSI_PBL_AFFIL_DESC," +
      " P2_DD_DISTRIBUTION_SOURCE_NAME, DESCRIPTION as P2_DESCRIPTION, t.P2_NETWORK_MATCH_FLAG," +
      " case " +
      "     when d.rec_key is null " +
      "     then 'No'" +
      "     when d.rec_key is not null and d.P2_NETWORK_MATCH_FLAG = 'Yes'  " +
      "     then 'Yes with one match'" +
      "     else 'Yes'" +
      "  end as duplicate_records" +
      "  from  stg4_scarborough_station_map t " +
      "  left outer join " +
      "  ( select rec_key, count(*) as rec_count, max(P2_NETWORK_MATCH_FLAG) as P2_NETWORK_MATCH_FLAG  " +
      "   from stg4_scarborough_station_map group by 1 having count(*) > 1 ) d" +
      "   on d.rec_key = t.rec_key order by rec_key, P2_DD_DISTRIBUTION_SOURCE_NAME")

    report_df.coalesce(1).write.mode(SaveMode.Overwrite).format("com.databricks.spark.csv").option("header", "true").save(workOutputDir+"distinct_network_keys.xls")

    val comparison_df = spark.sql("select  min(t.rec_key) as rec_key," +
      "     MARKET_NAME, MARKET_TYPE, MARKET_CODE, DMA_ID, PRIMARYSTUDYID," +
      "     substr( DESCRIPTION, 0, instr(upper(DESCRIPTION), ')')+1 ) as DESCRIPTION," +
      "     SCARBNETWORKKEY, NETWORKNAME, CALLLETTER, CHANNELNUMBER," +
      "     STATIONID, STATIONNAME, DD_STATION_CODE, DD_CALL_LETTERS," +
      "     DD_FIRST_NSI_PBL_AFFIL_S_NAME, DD_FIRST_NSI_PBL_AFFIL_DESC," +
      "     DD_DISTRIBUTION_SOURCE_NAME," +
      "     substr( DESCRIPTION, 0, instr(upper(DESCRIPTION), ')')+1 ) as DD_DESCRIPTION," +
      "     NETWORK_MATCH_FLAG, DD_STATION_ORIGIN, DD_STATION_ORIGIN_MARKET_NAME," +
      "     IN_MARKET_STATION_FLAG,P2_STATIONID, P2_STATIONNAME," +
      "     P2_DD_STATION_CODE, P2_DD_CALL_LETTERS, P2_FIRST_NSI_PBL_AFFIL_S_NAME," +
      "     P2_DD_FIRST_NSI_PBL_AFFIL_DESC,P2_DD_DISTRIBUTION_SOURCE_NAME," +
      "     substr( DESCRIPTION, 0, instr(upper(DESCRIPTION), ')')+1 ) as P2_DESCRIPTION," +
      "     t.P2_NETWORK_MATCH_FLAG,     " +
      "     case " +
      "       when d.rec_key is null " +
      "       then 'No'" +
      "       when d.rec_key is not null and d.P2_NETWORK_MATCH_FLAG = 'Yes'" +
      "       then 'Yes with one match'" +
      "       else 'Yes'" +
      "     end as duplicate_records" +
      "    from  stg4_scarborough_station_map t " +
      "    left outer join  " +
      "     ( select rec_key, count(*) as rec_count, max(P2_NETWORK_MATCH_FLAG) as P2_NETWORK_MATCH_FLAG " +
      "      from stg4_scarborough_station_map  group by 1 having count(*) > 1 ) d " +
      "    on d.rec_key = t.rec_key " +
      "     group by  MARKET_NAME, MARKET_TYPE, MARKET_CODE, DMA_ID, PRIMARYSTUDYID, DESCRIPTION,SCARBNETWORKKEY," +
      "     NETWORKNAME, CALLLETTER, CHANNELNUMBER,STATIONID,STATIONNAME,DD_STATION_CODE," +
      "     DD_CALL_LETTERS, DD_FIRST_NSI_PBL_AFFIL_S_NAME, DD_FIRST_NSI_PBL_AFFIL_DESC," +
      "     DD_DISTRIBUTION_SOURCE_NAME, DESCRIPTION, NETWORK_MATCH_FLAG, DD_STATION_ORIGIN," +
      "     DD_STATION_ORIGIN_MARKET_NAME, IN_MARKET_STATION_FLAG," +
      "     P2_STATIONID, P2_STATIONNAME, P2_DD_STATION_CODE, P2_DD_CALL_LETTERS, P2_FIRST_NSI_PBL_AFFIL_S_NAME, P2_DD_FIRST_NSI_PBL_AFFIL_DESC," +
      "     P2_DD_DISTRIBUTION_SOURCE_NAME, DESCRIPTION, t.P2_NETWORK_MATCH_FLAG, " +
      "     case when d.rec_key is null then 'No'  when d.rec_key is not null and d.P2_NETWORK_MATCH_FLAG = 'Yes'" +
      "      then 'Yes with one match'  else 'Yes' " +
      "     end order by 1, P2_DD_DISTRIBUTION_SOURCE_NAME")

    comparison_df.coalesce(1).write.mode(SaveMode.Overwrite).format("com.databricks.spark.csv").option("header", "true").save(workOutputDir+"comparison_results.xls")

    println("Station Mapping Job Completed : "+LocalDateTime.now())

  }
}